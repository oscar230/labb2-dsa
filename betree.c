
/****************************************************************************/
/* DSA tree program example   D.F. ROSS                                     */
/****************************************************************************/

/****************************************************************************/
/* include files and  global data objects                                   */
/****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/****************************************************************************/
/* define constants & types                                                 */
/****************************************************************************/
#define ARRLEN    100
#define NULLREF   NULL

/****************************************************************************/
/* tree element definition (this is hidden!)                                */
/****************************************************************************/
typedef struct treenode * treeref;

typedef struct treenode {
        int        value;
        treeref    LC;
        treeref    RC;
        } treenode;

/****************************************************************************/
/* define tree instance                                                     */
/****************************************************************************/
static treeref T     = (treeref) NULLREF;

/****************************************************************************/
/* define tree array                                                        */
/****************************************************************************/
static treeref treearray[ARRLEN];

/****************************************************************************/
/* define module variables                                                  */
/****************************************************************************/
static int infomode=0;                          /* demo/info mode           */
static int bstmode =0;                          /* AVL / BST mode           */

/****************************************************************************/
/*  basic operations on the tree                                            */
/****************************************************************************/
static int      is_empty(treeref T)             { return T == NULLREF;     }

static int      get_value(treeref T)            { return T->value;         }
static treeref  get_LC(treeref T)               { return T->LC;            }
static treeref  get_RC(treeref T)               { return T->RC;            }

static treeref  set_value(treeref T,  int v)    { T->value  = v; return T; }
static treeref  set_LC(treeref T, treeref L)    { T->LC     = L; return T; }
static treeref  set_RC(treeref T, treeref R)    { T->RC     = R; return T; }
/****************************************************************************/
/* create and initialise an element in the tree                             */
/****************************************************************************/

static treeref create_node(int v)
{
  return set_RC(
             set_LC(
                   set_value(malloc(sizeof(treenode)), v),
             NULLREF),
          NULLREF);
}

/****************************************************************************/
/* basic operations on the tree                                             */
/****************************************************************************/
/* LC, Node, RC - a RECURSIVE view of the tree                              */
/****************************************************************************/
static treeref node(treeref T)             { return T;         }
static treeref LC(treeref T)               { return get_LC(T); }
static treeref RC(treeref T)               { return get_RC(T); }

/****************************************************************************/
/* FIND the height of the tree                                              */
/****************************************************************************/
static int max(int a, int b) {
   return a>b ? a : b;
}

static int b_height(treeref T)
{
   if(is_empty(T)){
      return 0;
   }else{
      return max(b_height(RC(T)), b_height(LC(T))) + 1;  
   }
}
   
/****************************************************************************/
/* Switch between demo and info (stepwise) mode                             */
/****************************************************************************/
static void b_set_run_mode(int m) { infomode = m; }

/****************************************************************************/
/* Switch between AVL and BST mode                                          */
/****************************************************************************/
static void b_set_bst_mode(int m) { bstmode = m;  }

/****************************************************************************/
/* AVL TREE help functions ROTATION FUNCTIONS                               */
/****************************************************************************/
static void b_disp_2D();

static void p_SLR(treeref T) {

  if (!is_empty(T) && infomode) {
     b_disp_2D();
     printf("\n *************************************** ");
     printf("\n *** SLR at node %2d                  *** ", get_value(T));
     printf("\n *************************************** ");
     }
  }
  
static void p_SRR(treeref T) {

  if (!is_empty(T) && infomode) {
     b_disp_2D();
     printf("\n *************************************** ");
     printf("\n *** SRR at node %2d                  *** ", get_value(T));
     printf("\n *************************************** ");
     }
  }
  
static void p_DLR(treeref T) { 

  if (!is_empty(T) && infomode) {
     printf("\n *************************************** ");
     printf("\n *** DLR(T) is SRR(RC(T)) + SLR(T)   *** ");
     printf("\n *************************************** ");
     printf("\n *** DLR at node %2d is               *** ", get_value(T));
     printf("\n *** SRR at node %2d + SLR at node %2d *** ", 
            get_value(RC(T)), get_value(T));
     printf("\n *************************************** ");
     }
  }

static void p_DRR(treeref T) {

  if (!is_empty(T) && infomode) {
     printf("\n *************************************** ");
     printf("\n *** DRR(T) is SLR(LC(T)) + SRR(T)   *** ");
     printf("\n *************************************** ");
     printf("\n *** DRR at node %2d is               *** ", get_value(T));
     printf("\n *** SLR at node %2d + SRR at node %2d *** ",
            get_value(LC(T)), get_value(T));
     printf("\n *************************************** ");
     }
  }



static treeref SLR(treeref T) {
   p_SLR(T);
   //SINGLE LEFT ROTATION
   treeref tmp = RC(T);
   set_RC(T, LC(tmp));
   set_LC(tmp, T);
   return tmp;
}

static treeref SRR(treeref T) {
   p_SRR(T);   
   //SINGLE RIGHT ROTATION   
   treeref tmp = LC(T);
   set_LC(T, RC(tmp));
   set_RC(tmp, T);
   return tmp;
}

static treeref DLR(treeref T) { 
   p_DLR(T);
   //DOUBLE LEFT ROTATION
   set_RC(T, SRR(RC(T)));
   return SLR(T);
}

static treeref DRR(treeref T) {
   p_DRR(T);
   //DOUBLE RIGHT ROTATION
   set_LC(T, SLR(LC(T)));
   return SRR(T);
}

/****************************************************************************/
/* AVL BALANCE FUNCTION                                                     */
/****************************************************************************/

//Post: difference in height
static int HDiff(treeref T){

   return b_height(LC(T)) - b_height(RC(T));
}

static treeref balance(treeref T){
   treeref arot = NULLREF;
   if(is_empty(T)){
      return T;
   }
   int diff = HDiff(T);
   
   if(diff > 1){
      //Left
      if(HDiff(get_LC(T)) > 0){
         //Left
         arot = SRR(T);
      }else{
         //Right
         arot = DRR(T);
      }
   }else if(diff < -1){
      //Right
      if(HDiff(get_RC(T)) < 0){
         arot = SLR(T);
      }else{
         arot = DLR(T);
      }
   }else{
      arot = T;
   }
   return arot;
}

/****************************************************************************/
/* CONStruct a new tree from a LC, Node and RC                              */
/****************************************************************************/
static treeref cons(treeref LC, treeref N, treeref RC) {
      set_LC(N, LC);
      set_RC(N, RC);
      if(!bstmode){
        N = balance(N);
      }
      return N;
   }

/****************************************************************************/
/* ADD to the tree in AVL - BST order + balance                             */
/****************************************************************************/

static treeref b_add(treeref T, int v)
{
  return is_empty(T)         ? create_node(v)
    : v < get_value(node(T)) ? cons(b_add(LC(T), v), node(T), RC(T))
    : v > get_value(node(T)) ? cons(LC(T), node(T), b_add(RC(T), v))
    :                          T;
}

/****************************************************************************/
/* REMove an element from the tree / BST order                              */
/****************************************************************************/
static treeref b_rem(treeref T, int v);
static treeref minT(treeref T);
static treeref maxT(treeref T);

static treeref minT(treeref T){
   treeref tmp = T;
   while(!is_empty(LC(tmp))){
      tmp = LC(tmp);
   }
   return tmp;
}

static treeref maxT(treeref T){
   treeref tmp = T;
   while(!is_empty(RC(tmp))){
      tmp = RC(tmp);
   }
   return tmp;
}

static treeref b_rem(treeref T, int v)
{
   if (is_empty(T)){
      //No tree :(
      return T;
   }else{
      //If there exists a tree
      if (v < get_value(T)){
         //v less than root value
         set_LC(T, b_rem(LC(T), v));
         T = balance(T); //Balance
      }else if (v > get_value(T)){
         //v grater than root value
         set_RC(T, b_rem(RC(T), v));
         T = balance(T); //Balance
      }else{ 
         //v == root value then remove root

         //T with one or no child 
         if (is_empty(LC(T))){
            treeref tmp = RC(T); 
            free(T);
            return tmp;
         }else if(is_empty(RC(T))){ 
            treeref tmp = LC(T); 
            free(T); 
            return tmp;
         }

         //T with two children
         if((b_height(T->LC) - b_height(T->RC)) >= 0){
            treeref tmp = maxT(LC(T));
            // Copy the inorder successor's content to this node
            set_value(T, get_value(tmp)); 
            // Delete the inorder successor
            set_LC(T, b_rem(LC(T), get_value(tmp)));
            T = balance(T); //Balance
         }else if((b_height(T->RC) - b_height(T->LC)) >= 0){
            treeref tmp = minT(RC(T));
            // Copy the inorder successor's content to this node 
            set_value(T, get_value(tmp)); 
            // Delete the inorder successor 
            set_RC(T, b_rem(RC(T), get_value(tmp)));
            T = balance(T); //Balance
         }
      }
      return T; 
   }
}

/****************************************************************************/
/* FIND an element in the BST (Binary Search Tree)                          */
/****************************************************************************/
static int b_findb(treeref T, int v)
{
   if(is_empty(T)){
      //Does not exist
      return 0;
   }else if(is_empty(T) || get_value(T) == v){
      //T = NULL or T->Value = v
      return get_value(T);
   }else if(get_value(T) < v){
      //v greater than T->value
      return b_findb(get_RC(T), v);
   }else{
      //v less than T->value
      return b_findb(get_LC(T), v);
   }
}

/****************************************************************************/
/* FIND the number of elements in the tree (cardinality)                    */
/****************************************************************************/
int b_count(treeref T)
{
    int count = 1;
    if(!is_empty(LC(T))){
      if (!is_empty(LC(T))) {
         count += b_count(LC(T));
      }
    }
   if(!is_empty(RC(T))){
      if (!is_empty(RC(T))) {
         count += b_count(RC(T));
      }
   }
    return count;
}

static int b_size(treeref T)
{  
   int count = 0;
    if (!is_empty(T)) {
        count = b_count(T);
    }
    return count;
}

/****************************************************************************/
/* display the tree ELEMENT                                                 */
/****************************************************************************/
static void b_disp_el(treeref T)
{
   if (!is_empty(T)) printf(" %2d ", get_value(node(T)));
   else printf("  * ");
}

/****************************************************************************/
/* display the tree (pre-order)                                             */
/****************************************************************************/
static void b_disp_pre(treeref T)
{
   //Root left right
   if(!is_empty(T)){
      b_disp_el(node(T));
      b_disp_pre(LC(T));
      b_disp_pre(RC(T));
   }
}

/****************************************************************************/
/* display the tree (in-order)                                              */
/****************************************************************************/
static void b_disp_in(treeref T)
{
  if (!is_empty(T)) {
     b_disp_in(LC(T));
     b_disp_el(node(T));
     b_disp_in(RC(T));
     }
}
/****************************************************************************/
/* display the tree (post-order)                                            */
/****************************************************************************/
static void b_disp_post(treeref T)
{
   //Left right root
   if(!is_empty(T)){
      b_disp_post(LC(T));
      b_disp_post(RC(T));
      b_disp_el(node(T));
   }
}

/****************************************************************************/
/* display the treearray array                                              */
/****************************************************************************/
static void b_disp_array()
{
   int amount = 0;
   for(int i = 0; i <= b_height(T); i++){
      amount += pow(2, i);
   }
   amount++; //Correct for that the trees root node starts at array[1]
   for(int i = 1; i < amount; i++){
      if(is_empty(treearray[i])){
         printf(" [*]");
      }else{
         printf(" [%d]", get_value(treearray[i]));
      }
   }
}

/****************************************************************************/
/* display the TREE information                                             */
/****************************************************************************/
static void print_treeinfo() {
   printf("\n ---------------------------------------");
   printf("\n TREE:        nodes: %2d   height: %d ", b_size(T), b_height(T));
   printf("\n ---------------------------------------");
   printf("\n inorder:   "); b_disp_in(T);
   printf("\n preorder:  "); b_disp_pre(T);
   printf("\n postorder: "); b_disp_post(T);
   printf("\n Treearray: "); b_disp_array();
   printf("\n ---------------------------------------");
   printf("\n");
}

/****************************************************************************/
/* Tree to array via a treearray (breadth-first search)                     */
/****************************************************************************/
/* Transforms a binary tree to a sequence (including NULL (*) references)   */
/* e.g.  the following tree:                    5                           */
/*                                      2              7                    */
/*                                  *       3      6        *               */
/*                                                                          */
/* becomes: [5] [2] [7] [*] [3] [6] [*]                                     */
/****************************************************************************/
static void T2Q(treeref T, int qpos){
   //Root
   treearray[qpos] = T;
   //Right
   if(!is_empty(RC(T))){
      T2Q(RC(T), qpos * 2 + 1);
   }
   //Left
   if(!is_empty(LC(T))){
      T2Q(LC(T), qpos * 2);
   }
}

/****************************************************************************/
/* display the tree in 2D                                                   */
/****************************************************************************/
/* step 1: transform the tree to an array (Q) using T2Q() above             */
/* e.g. array [5] | [2] [7] | [*] [3] [6] [*]   | etc.                      */
/*      index (1) | (2) (3) | (4) (5) (6) (7)   | (8) ...                   */
/*      level (1) | (2) (2) | (3) (3) (3) (3)   | (4) ...                   */
/* step 2: then print the nodes at each level of the tree to give           */
/*                             5                                            */
/*                     2              7                                     */
/*                *        3      6        *                                */
/****************************************************************************/
static void b_disp_2D()
{
   int amount = 0;
   
   if(!is_empty(T)){
      //Set all to NULL in treearray
      memset(treearray, '\0', sizeof(treeref) * ARRLEN);
      //Exec T2Q
      T2Q(T, 1);

      if(infomode){
         //Informode
         print_treeinfo();
      }
      //Calculate amount of nodes including empty ones
      for(int i = 0; i <= b_height(T); i++){
         amount += pow(2, i);
      }
      amount++; //Correct for that the trees root node starts at array[1]

      //Print array
      for(int i = 0; i <= b_height(T) - 1; i++){ //Level
         for(int j = 0; j < (int)pow(2, i); j++){ //Node column
            for(int k = 0; k < b_height(T)-i+1; k++){
               printf("  ");
            }
            if(is_empty(treearray[(int)pow(2, i)+j])){
               printf("*");
            }else{
               printf("%d", get_value(treearray[(int)pow(2, i)+j]));
            }
         }
         printf("\n");
      }
   }else{
      printf("\n *** Tree is empty ");
   }
}

/****************************************************************************/
/* public operations on the tree                                            */
/****************************************************************************/
void be_disp_2D()                { b_disp_2D();          }
void be_add(int v)               { T = b_add(T, v);      }
void be_rem(int v)               { T = b_rem(T, v);      }
int  be_is_memberb(int v)        { return b_findb(T, v); }
void be_set_run_mode(int v)      { b_set_run_mode(v);    }
void be_set_bst_mode(int v)      { b_set_bst_mode(v);    }
/****************************************************************************/
/* end of basic functions                                                   */
/****************************************************************************/